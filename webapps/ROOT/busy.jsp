
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

Please wait until your results are computed. Click
<a href="${requestSynchronizer.resultURI}">here</a>
if your browser does not support redirects.


<head>
  <meta http-equiv="refresh" content="1; URL=${requestSynchronizer.resultURI}">
</head>
