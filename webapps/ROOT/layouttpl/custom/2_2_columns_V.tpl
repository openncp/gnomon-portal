<div class="columns-2" id="content-wrapper">
<table id="layout-grid" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" class="lfr-column thirty" id="column-2">$processor.processColumn("column-1")</td>
    <td valign="top" >
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top" class="lfr-column seventy" id="column-1">$processor.processColumn("column-2")</td>
        </tr>
        <tr>
          <td valign="top" class="lfr-column fifty" id="column-3">$processor.processColumn("column-3")</td>
        </tr>
      </table></td>
  </tr>
</table>
</div>