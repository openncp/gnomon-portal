<div class="columns-1_2_30-70" id="content-wrapper">
	<table id="layout-grid">
	<tr>
	  <td colspan="2" valign="top" class="lfr-column hundred" id="column-1">$processor.processColumn("column-1")</td>
	  </tr>
	<tr>
		<td class="lfr-column thirty" id="column-2" valign="top">
			$processor.processColumn("column-2")		</td>
		<td class="lfr-column seventy" id="column-3" valign="top">
			$processor.processColumn("column-3")		</td>
	</tr>
	</table>
</div>