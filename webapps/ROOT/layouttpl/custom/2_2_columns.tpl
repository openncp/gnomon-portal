<div class="columns-2-2" id="content-wrapper">
	<table id="layout-grid">
	<tr>
		<td class="lfr-column seventy" colspan="2" id="column-1" valign="top">
			$processor.processColumn("column-1")
		</td>
		<td class="lfr-column thirty" id="column-4" valign="top">
			$processor.processColumn("column-4")
		</td>
	</tr>
	<tr>
		<td class="lfr-column fifty" id="column-2" valign="top">
			$processor.processColumn("column-2")
		</td>
		<td class="lfr-column fifty" colspan="2" id="column-3" valign="top">
			$processor.processColumn("column-3")
		</td>
	</tr>
	</table>
</div>