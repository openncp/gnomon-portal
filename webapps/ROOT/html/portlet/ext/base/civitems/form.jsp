<%@ include file="/html/portlet/ext/base/civitems/init.jsp" %>
<%@ page import="com.ext.sql.StrutsFormFields" %>
<%@ page import="javax.portlet.RenderResponse" %>
<%@ page import="com.liferay.portal.util.WebKeys" %>
<%@ page import="com.ext.util.CommonDefs" %>

<%
String metadataClassId = request.getParameter("metadataClassId");
if (Validator.isNull(metadataClassId))
	metadataClassId = (String)request.getAttribute("metadataClassId");

String loadaction = (String)request.getAttribute("loadaction");
String metaDataClass = (String)request.getAttribute("metaDataClass");
//String loadaction = (String)request.getParameter("loadaction");
String formUrl = "/ext/civitems/update?actionURL=true" ;
String buttonText = "gn.button.update";
String titleText = "bs.civitems.action.update";
String readOnlyFlag="";
if (!Validator.isNull(loadaction) && loadaction.equals("delete"))
{
 	formUrl = "/ext/civitems/delete?actionURL=true" ;
 	buttonText = "gn.button.delete";
 	titleText = "bs.civitems.action.delete";
 	readOnlyFlag="true";
}
else if (!Validator.isNull(loadaction) && loadaction.equals("add"))
{
	formUrl = "/ext/civitems/add?actionURL=true" ;
	buttonText = "gn.button.add";
	titleText = "bs.civitems.action.add";
}
else if (!Validator.isNull(loadaction) && loadaction.equals("trans"))
{
	formUrl = "/ext/civitems/add?actionURL=true" ;
	buttonText = "gn.button.add-translation";
	titleText = "bs.civitems.action.add-translation";
}
%>

<h1 class="title1"><%= LanguageUtil.get(pageContext, titleText) + " : " + LanguageUtil.get(pageContext, metaDataClass) %></h1>

<html:errors property="<%= org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE %>"/>


<html:form action="<%= formUrl %>" method="post" enctype="multipart/form-data" styleClass="uni-form">
<input type="hidden" name="metaDataClass" value="<%= metaDataClass %>">
<input type="hidden" name="loadaction" value="<%= loadaction %>">
<c:if test="<%=Validator.isNotNull(redirect) %>">
<input type="hidden" name="redirect" value="<%= redirect %>">
</c:if>
<%-- if (!Validator.isNull(loadaction) && loadaction.equals("add")) { --%>
<input type="hidden" name="metadataClassId" value="<%= metadataClassId %>">
<%-- }  --%>


<tiles:insert page="/html/portlet/ext/struts_includes/struts_div_fields.jsp" flush="true">
<tiles:put name="formName" value="BsCivItemForm"/>
</tiles:insert>

<% if (Validator.isNotNull(metaDataClass)) {  %>
<tiles:insert page="/html/portlet/ext/struts_includes/metaData_div.jsp" flush="true">
	<tiles:put name="formName" value="BsCivItemForm"/>
	<tiles:put name="className"><%= metaDataClass %></tiles:put>
	<% com.ext.portlet.base.civitems.BsCivItemForm formBean = (com.ext.portlet.base.civitems.BsCivItemForm) request.getAttribute("BsCivItemForm");
	   if (formBean != null && formBean.getMainid() != null) {%>
	<tiles:put name="primaryKey" value="<%= formBean.getMainid().toString() %>"/>
	<tiles:put name="readOnlyStyle" value="block"/>
	<tiles:put name="readOnly" value="<%=readOnlyFlag %>"/>
	
	<% } %>
</tiles:insert>
<% }  %>

<div class="button-holder">
<html:submit styleClass="portlet-form-button"><%= LanguageUtil.get(pageContext, buttonText ) %></html:submit>

<logic:notEqual name="BsCivItemForm" property="lang" value="<%= defLang %>">
	<c:if test="<%=!loadaction.equals("trans")%>">
		<tiles:insert page="/html/portlet/ext/struts_includes/button.jsp" flush="true">
		  <tiles:put name="action"  value="/ext/civitems/delete" />
		  <tiles:put name="buttonName" value="deleteButton" />
		  <tiles:put name="buttonValue" value="gn.button.delete-translation" />
		  <tiles:put name="formName"   value="BsCivItemForm" />
		  <tiles:put name="confirm" value="gn.messages.are-you-sure-you-want-to-delete-this-translation"/>
		  <tiles:put name="actionParam" value="deleteDetail"/>
		  <tiles:put name="actionParamValue" value="1"/>
		</tiles:insert>
	</c:if>
</logic:notEqual>

<c:choose>
	<c:when test="<%=Validator.isNotNull(redirect)%>">
	<input class="portlet-form-button" type="button" value='<%= LanguageUtil.get(pageContext, "cancel") %>' onClick="self.location = '<%= redirect %>';">
	</c:when>
	<c:otherwise>
	<input class="portlet-form-button" type="button" value='<%= LanguageUtil.get(pageContext, "back") %>' onClick="history.go(-1);">
	</c:otherwise>
</c:choose>
</div>
</html:form>


<c:if test="<%=loadaction.equals("edit")%>">
	<br>
	<p><h3 class="title2"><%= LanguageUtil.get(pageContext, "gn.translations" ) %></h3><p>
	<tiles:insert page="/html/portlet/ext/struts_includes/translationButtons.jsp" flush="true">
		<tiles:put name="editAction"  value="/ext/civitems/load" />
		<tiles:put name="editActionParam" value="loadaction"/>
	    <tiles:put name="editActionParamValue" value="edit"/>
	    <tiles:put name="addAction"  value="/ext/civitems/load" />
		<tiles:put name="addActionParam" value="loadaction"/>
	    <tiles:put name="addActionParamValue" value="trans"/>
	</tiles:insert>
</c:if>
