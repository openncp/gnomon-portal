<%@ include file="/html/portlet/ext/parties/init.jsp" %>
<%@ page import="com.ext.sql.StrutsFormFields" %>

<tiles:insert page="/html/portlet/ext/struts_includes/titleData.jsp" flush="true"/>

<h3 class="title1"><%= LanguageUtil.get(pageContext, "parties.manager.form.add-webpage-address") %></h3>
<%
StrutsFormFields new_field=null;
Vector fields=new Vector();
String curFormName="WebpageAddressForm";
fields.addElement(new StrutsFormFields("mainid","mainid","text",true));
fields.addElement(new StrutsFormFields("partyid","partyid","text",true));
new_field = new StrutsFormFields("url","parties.addresses.webpage.url","text",false);
new_field.setRequired(true);
fields.addElement(new_field);
fields.addElement(new StrutsFormFields("description","parties.addresses.webpage.description","text",false));
request.setAttribute("_vector_fields", fields);
%>

<html:form action="/ext/parties/manager/addWebpageAddress?actionURL=true" method="post">
<tiles:insert page="/html/portlet/ext/struts_includes/struts_fields.jsp" flush="true">
	<tiles:put name="formName"><%= curFormName %></tiles:put>
	<tiles:put name="attributeName" value="_vector_fields"/>
</tiles:insert>
<html:submit styleClass="portlet-form-button"><%= LanguageUtil.get(pageContext, "parties.button.add") %></html:submit> 
</html:form>

<%@ include file="/html/portlet/ext/parties/manager/addressFooter.jsp" %>


