<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<h2>JPivot is busy ...</h2>

Please wait until your results are computed. Click
<a href="<c:out value="${requestSynchronizer.resultURI}"/>">here</a>
if your browser does not support redirects.

