## ---------- Common variables ---------- ##

#set ($theme_display = $themeDisplay)
#set ($portlet_display = $portletDisplay)

#set ($css_class = $theme_display.getColorScheme().getCssClass())

#if ($layout)
	#if ($layout.getGroup().isStagingGroup())
		#set ($css_class = $css_class + " staging")
	#end
#end

#set ($css_folder = "$theme_display.getPathThemeCss()")
#set ($images_folder = "$theme_display.getPathThemeImages()")
#set ($javascript_folder = "$theme_display.getPathThemeJavaScript()")
#set ($templates_folder = "$theme_display.getPathThemeTemplates()")

#set ($full_css_path = $fullCssPath)
#set ($full_templates_path = $fullTemplatesPath)

#set ($css_main_file = "$css_folder/main.css?companyId=$theme_display.getCompanyId()&languageId=$languageUtil.getLanguageId($locale)&themeId=$theme_display.getThemeId()&colorSchemeId=$theme_display.getColorSchemeId()")
#set ($js_main_file = "$javascript_folder/javascript.js")

#set ($company_id = $company.getCompanyId())
#set ($company_name = $company.getName())
#set ($company_logo = $theme_display.getCompanyLogo())
#set ($company_logo_height = $theme_display.getCompanyLogoHeight())
#set ($company_logo_width = $theme_display.getCompanyLogoWidth())
#set ($company_url = $theme_display.getURLHome())

#set ($user_id = $user.getUserId())
#set ($is_default_user = $user.isDefaultUser())
#set ($user_first_name = $user.getFirstName())
#set ($user_middle_name = $user.getMiddleName())
#set ($user_last_name = $user.getLastName())
#set ($user_name = $user.getFullName())
#set ($is_male = $user.isMale())
#set ($is_female = $user.isFemale())
#set ($user_birthday = $user.getBirthday())
#set ($user_email_address = $user.getEmailAddress())
#set ($language_id = $user.getLanguageId())
#set ($time_zone = $user.getTimeZoneId())
#set ($user_greeting = $user.getGreeting())
#set ($user_comments = $user.getComments())
#set ($user_login_ip = $user.getLoginIP())
#set ($user_last_login_ip = $user.getLastLoginIP())
#set ($user_group = $user.getGroup())
#set ($user_organization = $user.getOrganization())
#set ($user_location = $user.getLocation())

#set ($is_signed_in = $theme_display.isSignedIn())

#set ($group_id = $theme_display.getPortletGroupId())

## ---------- URLs ---------- ##

#set ($show_add_content = $theme_display.isShowAddContentIcon())

#if ($show_add_content)
	#set ($add_content_text = $languageUtil.get($company_id, $locale, "add-content"))
	#set ($add_content_url = $theme_display.getURLAddContent())

	#set ($layout_text = $languageUtil.get($company_id, $locale, "layout"))
	#set ($layout_url = $theme_display.getURLLayoutTemplates())
#end

#set ($show_home = $theme_display.isShowHomeIcon())

#if ($show_home)
	#set ($home_text = $languageUtil.get($company_id, $locale, "home"))
	#set ($home_url = $theme_display.getURLLayoutTemplates())
#end

#set ($show_my_account = $theme_display.isShowMyAccountIcon())

#if ($show_my_account)
	#set ($my_account_text = $languageUtil.get($company_id, $locale, "my-account"))
	#set ($my_account_url = $theme_display.getURLMyAccount())
#end

#set ($show_page_settings = $theme_display.isShowPageSettingsIcon())

#if ($show_page_settings)
	#set ($page_settings_text = $languageUtil.get($company_id, $locale, "page-settings"))
	#set ($page_settings_url = $theme_display.getURLPageSettings().toString())
#end

#set ($show_sign_in = $theme_display.isShowSignInIcon())

#if ($show_sign_in)
	#set ($sign_in_text = $languageUtil.get($company_id, $locale, "sign-in"))
	#set ($sign_in_url = $theme_display.getURLSignIn())
#end

#set ($show_sign_out = $theme_display.isShowSignOutIcon())

#if ($show_sign_out)
	#set ($sign_out_text = $languageUtil.get($company_id, $locale, "sign-out"))
	#set ($sign_out_url = $theme_display.getURLSignOut())
#end

#if ($permissionChecker.isOmniadmin() && $portalUtil.isUpdateAvailable())
    #set ($update_available_url = $theme_display.getURLUpdateManager())
#end

## ---------- Page ---------- ##

#set ($the_title = $languageUtil.get($company_id, $locale, $tilesTitle))
#set ($selectable = $theme_display.isTilesSelectable())
#set ($is_maximized = $layoutTypePortlet.hasStateMax())
#set ($is_freeform = $themeDisplay.isFreeformLayout())

#if ($layout)
	#set ($page = $layout)

	#set ($is_first_child = $page.isFirstChild())
	#set ($is_first_parent = $page.isFirstParent())

	#set ($the_title = $languageUtil.get($company_id, $locale, $the_title, $page.getName($locale)))

	#if ($page.getType().equals("portlet"))
		#set ($is_portlet_page = true)
	#end

	#if ($is_portlet_page && $theme_display.isWapTheme())
		#set ($all_portlets = $layoutTypePortlet.getPortlets())
		#set ($column_1_portlets = $layoutTypePortlet.getAllPortlets("column-1"))
		#set ($column_2_portlets = $layoutTypePortlet.getAllPortlets("column-2"))
		#set ($column_3_portlets = $layoutTypePortlet.getAllPortlets("column-3"))
		#set ($column_4_portlets = $layoutTypePortlet.getAllPortlets("column-4"))
		#set ($column_5_portlets = $layoutTypePortlet.getAllPortlets("column-5"))

		#if ($layoutTypePortlet.hasStateMax())
			#set ($maximized_portlet_id = $layoutTypePortlet.getStateMaxPortletId())
		#end

	#end
#end

#if ($layout.getHTMLTitle($locale))
	#set ($the_title = $layout.getHTMLTitle($locale))
#end

#if ($pageTitle)
	#set ($the_title = $pageTitle)
#end

#if ($pageSubtitle)
	#set ($the_title = $the_title + " - " + $pageSubtitle)
#end

#if ($layouts)
	#set ($pages = $layouts)
#end

## ---------- Navigation ---------- ##

#if ($navItems)
	#set ($nav_items = $navItems)
	#set ($has_navigation = ($nav_items && $nav_items.size() > 0))
#end

## ---------- My places ---------- ##

#set ($my_places_text = $languageUtil.get($company_id, $locale, "my-places"))

## ---------- Includes ---------- ##

#if ($is_portlet_page && $theme_display.isWapTheme())
	#set ($dir_include = "/wap")
#else
	#set ($dir_include = "/html")
#end

#set ($bottom_ext_include = "$dir_include/common/themes/bottom-ext.jsp")
#set ($content_include = "$dir_include$tilesContent")
#set ($session_timeout_include = "$dir_include/common/themes/session_timeout.jsp")
#set ($sound_alerts_include = "$dir_include/common/themes/sound_alerts.jsp")
#set ($top_head_include = "$dir_include/common/themes/top_head.jsp")
#set ($top_messages_include = "$dir_include/common/themes/top_messages.jsp")

## ---------- Date ---------- ##

#set ($date = $dateUtil)

#set ($current_time = $date.getCurrentDate())
#set ($the_year = $date.getCurrentDate("yyyy", $locale))

## ---------- Custom init ---------- ##

#parse ("$full_templates_path/init_custom.vm")