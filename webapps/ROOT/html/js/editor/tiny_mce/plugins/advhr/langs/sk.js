/**
 * Slovak lang variables 
 * encoding: utf-8
 * 
 * @author Vladimir VASIL vvasil@post.sk
 *    
 * $Id$ 
 */  

tinyMCE.addToLang('',{
insert_advhr_desc : 'Vložiť/editovať vodorovný oddeľovač',
insert_advhr_width : 'Šírka',
insert_advhr_size : 'Výška',
insert_advhr_noshade : 'Nestieňovať'
});

