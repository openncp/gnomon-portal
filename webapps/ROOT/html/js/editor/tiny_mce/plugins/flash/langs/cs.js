/**
 * Czech lang variables
 * encoding: utf-8
 *
 * $Id$
 */

tinyMCE.addToLang('flash',{
title : 'Vložit / editovat Flash',
desc : 'Vložit / editovat Flash',
file : 'Flash soubor (.swf)',
size : 'Velikost',
list : 'Flash soubory',
props : 'Flash nastavení',
general : 'Obecné'
});
