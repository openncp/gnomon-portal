/**
 * Slovak lang variables 
 * encoding: utf-8
 * 
 * @author Vladimir VASIL vvasil@post.sk
 *    
 * $Id$ 
 */  

tinyMCE.addToLang('',{
insert_flash : 'Vložiť/editovať Flash Movie',
insert_flash_file : 'Flash súbor (.swf)',
insert_flash_size : 'Veľkosť',
insert_flash_list : 'Zoznam',
flash_props : 'Vlastnosti Flash'
});
