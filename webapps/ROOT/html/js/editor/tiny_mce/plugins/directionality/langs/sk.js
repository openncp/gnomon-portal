/**
 * Slovak lang variables 
 * encoding: utf-8
 * 
 * @author Vladimir VASIL vvasil@post.sk
 *    
 * $Id$ 
 */  

tinyMCE.addToLang('',{
directionality_ltr_desc : 'Smer z ľava doprava',
directionality_rtl_desc : 'Smer z prava doľava'
});

