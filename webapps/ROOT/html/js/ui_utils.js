function toggleById(id, returnState, displayType) {
	var obj = document.getElementById(id);

	if (returnState) {
		return toggleByObject(obj, returnState, displayType);
	}
	else {
		toggleByObject(obj, null, displayType);
	}
}

function toggleByIdSpan(obj, id) {
	var hidden = toggleById(id, true);
	var spanText = obj.getElementsByTagName("span");

	if (hidden) {
		spanText[0].style.display = "none";
		spanText[1].style.display = "";
	}
	else {
		spanText[0].style.display = "";
		spanText[1].style.display = "none";
	}
}

function toggleByObject(obj, returnState, displayType) {
	var hidden = false;
	var display = "block";

	if (displayType != null) {
		display = displayType;
	}

	if (obj != null) {
		if (!obj.style.display || !obj.style.display.toLowerCase().match("none")) {
			obj.style.display = "none";
		}
		else {
			obj.style.display = display;
			hidden = true;
		}
	}

	if (returnState) {
		return hidden;
	}
}